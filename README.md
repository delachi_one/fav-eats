# Fav Eats (A Demo App Developed by Golden Reggie Butler)

* [Summary](#summary)
* [Online Demo](#online-demo)

## Summary
This project is an exercise to exhibit my mobile first, front-end development/programming skills, using Anguluar.  You'll find the source files in the 'src/app' directory.


## Online Demo

A built demo version of this app can be located online.  Just point your browser on your mobile device or desktop to:

    http://devcloud.delachi.com/fav-eats/
