import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Subject } from 'rxjs/Subject';


@Injectable()

export class RestaurantService {

	_showLoader: boolean;
	_loaderVisibility: Subject<boolean> = new Subject<boolean>();

	constructor(private http: HttpClient) { 
		this._showLoader = false;
	}

	//: getRestaurants
	getRestaurants(): Observable<any> {
		return this.http
			.get('https://s3.amazonaws.com/br-codingexams/restaurants.json');
	}
	//#


	//: preLoadImages
	preLoadImages(bgImages): Promise<any[]> {
		return new Promise((resolve, reject) => {
	      
	      let images = bgImages;
			let imgcnt = 0;
	      

			for(let i = 0; i < images.length; i++) {
				let _img = new Image();
			  
				_img.src = images[i].backgroundImageURL;

				_img.onload = function() {
					imgcnt++;

					if(imgcnt === images.length) {
						resolve();
					}
				};

				_img.onerror = function() {
					reject();
				};
			}  

    	});
	}

}
