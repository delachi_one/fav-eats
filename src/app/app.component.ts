import { Component, OnInit, Input, SimpleChanges, OnChanges, NgZone} from '@angular/core';
import { DoCheck, trigger, state, animate, transition, style, Directive, ElementRef  } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from "@angular/common";
import { RestaurantService } from './restaurants/restaurant.service';
import { Store } from './restaurants/store';

declare var google: any;
var _window;

@Component({
  selector: 'fav-eats',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})

export class AppComponent {
  
	title = 'FavEats';
	errorLoading: boolean;
	headerTitle = 'Lunch Tyme';
	detailView: boolean;
	showLoader: boolean;
	showDevCredit: boolean;
	mapLoaded: boolean;
	store: Store;
	details:any = {};
	loadingMsg = "YUM!<br/>Loading Your FavEats&trade;";
	errorMsg = "BUMMER!<br/>Problem loading your<br/>FavEats&trade;&nbsp;&nbsp;Refresh & try again. :(";

	constructor(
		private FES: RestaurantService,
		public zone: NgZone,
		private location: Location
	) {}

 	//: ngOnInit
 	ngOnInit() {
 		

		window.scrollTo(0,0);
   	this.showLoader = true;


   	setTimeout(() => {
   		if(!this.store) {
   			this.displayErrorMsg(this.errorMsg);
   		}
   	},10000);

 		
		this.FES.getRestaurants()
 			.subscribe(
				store => {

					//:preload Images...
					this.FES.preLoadImages(store.restaurants)
						.then(() => { 

							if(!this.errorLoading) {
								//: allow scroll...
								_window = document.getElementsByTagName("body")[0];
								_window.style = 'overflow-y:scroll';

								
								//: bind to store...
								this.store = store.restaurants;
								
								setTimeout(() => {
									this.showLoader = false;
									this.showDevCredit = true;
									this.startDevelopedBy();
								},700);
							}
							

						});
					
				},
				err => {
					this.displayErrorMsg(this.errorMsg);
				}
			)
			
 	}

 	//: getRestaurantDetail
 	getRestaurantDetail(restaurant): void {

 		window.scrollTo(0,0);

 		this.mapLoaded = false;
 		this.detailView =  true;
 		this.details = restaurant;

 		let location = {lat: this.details.location.lat, lng: this.details.location.lng};

		let map = new google.maps.Map(document.getElementById('map'), {
			center: location,
			zoom: 13
		});

		let marker = new google.maps.Marker({
			position: location,
			map: map,
			title: this.details.name
        });

		//: hide loading gif when map is ready...
		google.maps.event.addListener(map,'tilesloaded',() => {
			this.zone.run(() => {
				this.mapLoaded = true;
				google.maps.event.clearListeners(map,'tilesloaded');
			});
		});
 	}

 	//: goBack
 	goBack(): void {
 		this.detailView = false;
 	}

 	//: displayErrorMsg
 	displayErrorMsg(msg): void {
 		this.loadingMsg = msg;
 		this.errorLoading = true;
 	}

 	//: startDevelopedBy
 	startDevelopedBy(): void {
 		setInterval(() => {
      	this.showDevCredit = true;

      	setTimeout(() => {
				this.showDevCredit = false;      		
      	},10000)

   	},15000);
 	}
}
