import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { Location } from "@angular/common";


import { AppComponent } from './app.component';
import { RestaurantService } from './restaurants/restaurant.service';


const appRoutes: Routes = [
   { path: 'restaurants', component: AppComponent },
   { path: 'restaurant/:id', component: AppComponent }
];


@NgModule({
   declarations: [
      AppComponent
   ],
   imports: [
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule,
      HttpClientModule,
      HttpModule,
      BrowserAnimationsModule
   ],
   providers: [RestaurantService],
   bootstrap: [AppComponent]
})
export class AppModule { }
